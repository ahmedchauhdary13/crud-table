# Getting Started with Create React App
All thats needed to get started is to run the command below to add node_modules

## Scripts(Run both in seperate terminals to begin)

npx json-server --watch db.json --port 4000
npm start

Runs the app in the development mode.\
Open [http://localhost:3000] to view it in your browser for front-end
Use [http://localhost:4000] to test back-end APIs

## Basic Usage

The App shows table-data fetched from db.json, you can 'add' new records by clicking on plus button at top left corner, the records are 'editable' and 'deleteable' with the help of the buttons in Actions column.
Double click to edit a row and press enter once you're done editing then click the save button to finally store data.

Drag the mouse over the coulmn name to see the options button, here you can 'sort', 'hide' and 'filter' data to your liking

